<?php
/**
 * Created by PhpStorm.
 * User: owner
 * Date: 6/4/2020
 * Time: 9:16 PM
 */

namespace App\Repositories;


use App\Models\Contact;
use Illuminate\Support\Facades\Auth;
use Prettus\Repository\Eloquent\BaseRepository;

class ContactRepository extends BaseRepository
{

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Contact::class;
    }

    public function registerUser(array $data){
        $data['password'] = bcrypt($data['password']);
        $user = $this->create($data);
        return $user;
    }

    public function create(array $attributes)
    {
        $user = Auth::user();

        return $user->contacts()->create($attributes);
    }
}
