<?php
/**
 * Created by PhpStorm.
 * User: owner
 * Date: 6/4/2020
 * Time: 9:16 PM
 */

namespace App\Repositories;


use App\User;
use Illuminate\Support\Facades\Auth;
use Prettus\Repository\Eloquent\BaseRepository;

class UserRepository extends BaseRepository
{

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return User::class;
    }

    public function registerUser($data){
        $data['password'] = bcrypt($data['password']);
        $user = $this->create($data);
        return $user;
    }


    public function getUserContacts(){
        $user = Auth::user();
        return $user->contacts()->get();
    }
}
