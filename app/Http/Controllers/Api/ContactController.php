<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\ContactStoreRequest;
use App\Http\Requests\ContactAccessRequest;
use App\Models\Contact;
use App\Repositories\ContactRepository;
use App\Repositories\UserRepository;
use App\Http\Resources\Contact as ContactResource;

class ContactController extends BaseController
{
    private $contactRep;
    private $userRep;

    public function __construct(ContactRepository $contactRepository, UserRepository $userRepository)
    {
        $this->contactRep = $contactRepository;
        $this->userRep = $userRepository;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contacts = $this->userRep->getUserContacts();

        return $this->sendResponse(ContactResource::collection($contacts), 'Contacts retrieved successfully.');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ContactStoreRequest $request)
    {
        $contact = $this->contactRep->create($request->all());

        return $this->sendResponse(new ContactResource($contact), 'Contact created successfully.', 201);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ContactAccessRequest $request, Contact $contact)
    {
        $contact = $this->contactRep->update($request->all(), $contact->id);

        return $this->sendResponse(new ContactResource($contact), 'Contact updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(ContactAccessRequest $request, Contact $contact)
    {
        $contact->delete();

        return $this->sendResponse([], 'Contact deleted successfully.');
    }
}
