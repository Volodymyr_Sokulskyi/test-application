docker-compose exec app php artisan migrate
docker-compose exec app php artisan passport:install
docker-compose exec app php artisan key:generate
docker-compose exec app php artisan config:cache
